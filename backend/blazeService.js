const express = require('express');
const cors = require('cors')
const app = express();
const path = require('path')
const bodyParser = require('body-parser');

app.use(cors());
app.use('/scores', express.static(path.join(__dirname, 'scores.json')))
app.use(bodyParser.json());

app.use('/login', (req, res) => {
    const credentials = req.body.credentials;
    if (credentials.username === '1' && credentials.password === '1'){
        res.send({
            token: 'test123'
        });
    }
});

app.listen(8197, ()=>{
    console.log('Server running on 8197...');
  });