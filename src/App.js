import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect } from 'react';
import Main from './Components/main';
import {BrowserRouter as Router,} from "react-router-dom";


function App() {
  return (
    <Router>
      <Main />
    </Router>
  );
}


export default App;



