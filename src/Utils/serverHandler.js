import axios from 'axios';

export const getActivities = ()=>{
    return axios.get(`http://localhost:8197/scores`);
}

export const loginUser = async (credentials) => {
    const res = await axios.post(`http://localhost:8197/login`, { credentials })
    console.log(res.data);
    return res.data;
}