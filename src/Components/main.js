import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useHistory 
  } from "react-router-dom";
  import React, { useState, useEffect } from 'react';
  import Activites from './activites'
  import Login from './Login';
  import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
  import companyLogo  from '../images/blazepod_logo_410x.png'

  const theme = createMuiTheme({
    overrides: {
      MuiMenuItem: { // For ListItem, change this to MuiListItem
        root: {
          color: 'white',
          backgroundColor: '#0d1c30',
          "&$selected": {       // this is to refer to the prop provided by M-UI
            backgroundColor: "gray", // updated backgroundColor
          },
        },
      },
    }
  });

const Main = () =>{
    const [token, setToken] = useState();
    const history = useHistory();
    if(!token) {
        history.push('/login')
    } else{
        history.push('/activites')
    }
    return <div style={{height:"100vh", backgroundColor: '#0d1c30', overflow:'hidden'}}>
    <ThemeProvider theme={theme}>
          <div style={{backgroundColor: '#111111', display:'flex'}}>
              <img src={companyLogo}/>
          {/* {links()} */}
          </div>

            <div style={{flexGrow:1, marginLeft: '20px'}}>
        <Switch>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/activites">
            <Activites />
          </Route>
          <Route path="/login">
            <Login setToken={setToken} />
          </Route>
        </Switch>
        </div>
    </ThemeProvider>
      </div>
}

export default Main;

function Home() {
    return <h2>Home</h2>;
  }
  
  function About() {
    return<div><h2>About</h2><p>this is Menny's assignment</p></div>;
  }

  function links(){
      return (
       <ul>
            <li>
              <Link to="/login">Login</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
            <li>
              <Link to="/activites">Activites</Link>
            </li>
            <li>
              <Link to="/allenai">Allen ai</Link>
            </li>
          </ul> );
  }
  