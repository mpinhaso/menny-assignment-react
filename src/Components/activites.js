import React, { useState, useEffect } from 'react';
import {List, MenuItem  , ListItemText, makeStyles } from '@material-ui/core';
import * as Server from '../Utils/serverHandler'
import ActivityGrafh from './activityGrafh'

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      maxWidth: 360,
      backgroundColor: theme.palette.background.paper,
      overflow:'auto',
      backgroundColor: '#0d1c30',
      '&::-webkit-scrollbar': {
        width: '10px'
      },
      '&::-webkit-scrollbar-track': {
        boxShadow: 'inset 0 0 6px #253a52',
        webkitBoxShadow: 'inset 0 0 6px #253a52'
      },
      '&::-webkit-scrollbar-thumb': {
        backgroundColor: '#0c7c96',
        outline: '1px solid #253a52'
      }
    },
  }));
  
  

  const Activites = () => {
    const classes = useStyles();
    const [activites, setActivites] = useState([]);
    const [selectedActivity, setSelectedActivity] = useState(null);
    //console.log(activites);
    useEffect(() => {
       Server.getActivities().then(res => {
        res.data.forEach((activity,i)=>activity.id = `${activity.activityKey}_${i}`)
        setActivites(res.data)
      })
    },[]);
  
    const activityClicked = (activity) => {
      setSelectedActivity(activity)
      console.log(activity);
    }
  
    return (
      <div style={{padding:'10px'}}>
        <h2 style={{color:'white', marginLeft:'30px'}}>Activites</h2>
        <div style={{display:'flex', height:'400px', backgroundColor: '#0e2037', minWidth: '200px'}}>
          <List dense className={classes.root}>
          {activites.map((activity, index) => {
            return (
              <MenuItem  key={activity.id} button onClick={() => activityClicked(activity)} selected={selectedActivity && activity.id === selectedActivity.id}>
                <ListItemText  primary={`-- ${activity.name} --`}  />
              </MenuItem >

            );
          })}
        </List>
        {selectedActivity?
        <div style={{flexGrow:1, borderRadius: 5, marginLeft: 25}}>
          <ActivityGrafh activity={selectedActivity}/>
        </div>: null
        }
      </div>
    </div>
    );
  
  }

  export default Activites;