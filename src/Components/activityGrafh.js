import React from 'react';
import { render } from 'react-dom';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

const ActivityGrafh = (props) => {

    const getReactions = ()=>{
      const reacions = props.activity.cycles? props.activity.cycles[0].players.map(player=>({name : player.playerUserData.name, data: player.reactions.map(r=>r.time)})):[]
      console.log(reacions);
      return reacions

    }

    const options = {
      chart: {
        type: 'spline',
        backgroundColor: '#0e2037',
        borderRadius:'10px'
      },
      title: {
        text: props.activity.name,
        style:  { "color": "white", "fontSize": "18px" }
      },
      yAxis: {
        title: {
            text: 'Reacions'
        },
        min: 0
      },
        series: getReactions()
      };

    return <HighchartsReact highcharts={Highcharts} options={options} />
}

export default ActivityGrafh;